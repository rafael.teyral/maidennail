﻿module maidennail.IncidentHttp

open Giraffe
open Microsoft.AspNetCore.Http
open FSharp.Control.Tasks.V2

let handlers : HttpFunc -> HttpContext -> HttpFuncResult =
    choose [
      POST >=> route "/incident" >=>
        fun next context ->
          task {
            let save = context.GetService<Incident.Add>()
            let serializer = context.GetJsonSerializer()
            let! bodyText = context.ReadBodyFromRequestAsync()
            let body = serializer.Deserialize<Incident.Incident> bodyText;
            return! json (save body) next context
          }

      GET >=> route "/incident" >=>
        fun next context ->
          let find = context.GetService<Incident.Get>()
          json (find()) next context

      PUT >=> routef "/incident/%s" (fun id ->
        fun next context ->
          text ("Update " + id) next context)
    ]
