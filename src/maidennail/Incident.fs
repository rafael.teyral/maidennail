﻿module maidennail.Incident

open System

type Mood = AWESOME | GOOD | BAD

type Incident = {
    Id: Guid
    Comment: string
    Mood: string
}

type Add = Incident -> Incident

type Get = unit -> Incident[]
