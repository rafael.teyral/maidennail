module maidennail.IncidentMongo

open Microsoft.Extensions.DependencyInjection
open MongoDB.Driver
open maidennail.Incident

let find (collection : IMongoCollection<Incident>) () : Incident[] =
  collection.Find(Builders.Filter.Empty).ToEnumerable() |> Seq.toArray

let save (collection : IMongoCollection<Incident>) (incident : Incident) : Incident =
  let incidents = collection.Find(fun x -> x.Id = incident.Id).ToEnumerable()

  match Seq.isEmpty incidents with
  | true -> collection.InsertOne incident
  | false ->
    let filter = Builders<Incident>.Filter.Eq((fun x -> x.Id), incident.Id)
    let update =
      Builders<Incident>.Update
        .Set((fun x -> x.Comment), incident.Comment)
        .Set((fun x -> x.Mood), incident.Mood)
    collection.UpdateOne(filter, update) |> ignore

  incident

type IServiceCollection with
  member this.AddIncidentMongoDB(collection : IMongoCollection<Incident>) =
    this.AddSingleton<Get>(find collection) |> ignore
    this.AddSingleton<Add>(save collection) |> ignore