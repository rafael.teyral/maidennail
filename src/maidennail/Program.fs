module maidennail.App

open System
open System.Collections
open System.Text.Json.Serialization
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Cors.Infrastructure
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Logging
open Microsoft.Extensions.DependencyInjection
open MongoDB.Driver
open Newtonsoft.Json
open maidennail.Incident
open maidennail.IncidentInMemory
open maidennail.IncidentMongo
open Giraffe
open maidennail.HttpHandlers
open maidennail.ideomatic


// ---------------------------------
// Web app
// ---------------------------------

let webApp =
    choose [
        subRoute "/api" (IncidentHttp.handlers)
        setStatusCode 404 >=> text "Not Found" ]

// ---------------------------------
// Error handler
// ---------------------------------

let errorHandler (ex : Exception) (logger : ILogger) =
    logger.LogError(ex, "An unhandled exception has occurred while executing the request.")
    clearResponse >=> setStatusCode 500 >=> text ex.Message

// ---------------------------------
// Config and Main
// ---------------------------------

let configureCors (builder : CorsPolicyBuilder) =
    builder.WithOrigins("http://localhost:8080")
           .AllowAnyMethod()
           .AllowAnyHeader()
           |> ignore

let configureApp (app : IApplicationBuilder) =
    let env = app.ApplicationServices.GetService<IHostingEnvironment>()
    (match env.IsDevelopment() with
    | true  -> app.UseDeveloperExceptionPage()
    | false -> app.UseGiraffeErrorHandler errorHandler)
        .UseHttpsRedirection()
        .UseCors(configureCors)
        .UseGiraffe(webApp)

let configureServices (services : IServiceCollection) =
    let mongo = MongoClient (Environment.GetEnvironmentVariable "MONGO_URL")
    let db = mongo.GetDatabase "incidents"
    services.AddCors()    |> ignore
    services.AddGiraffe() |> ignore
    services.AddIncidentInMemory(Hashtable()) |> ignore
    services.AddIncidentMongoDB(db.GetCollection<Incident>("incidents")) |> ignore

let configureLogging (builder : ILoggingBuilder) =
    builder.AddFilter(fun l -> l.Equals LogLevel.Error)
           .AddConsole()
           .AddDebug() |> ignore

[<EntryPoint>]
let main _ =
    WebHostBuilder()
        .UseKestrel()
        .UseIISIntegration()
        .Configure(Action<IApplicationBuilder> configureApp)
        .ConfigureServices(configureServices)
        .ConfigureLogging(configureLogging)
        .Build()
        .Run()
    0
