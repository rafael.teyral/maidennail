﻿module maidennail.IncidentInMemory

open System.Collections
open Microsoft.Extensions.DependencyInjection
open maidennail.Incident

let find (inMemory : Hashtable) () : Incident[] =
  inMemory.Values |> Seq.cast |> Array.ofSeq

let save (inMemory : Hashtable) (incident : Incident) : Incident =
  inMemory.Add(incident.Id, incident) |> ignore
  incident

type IServiceCollection with
  member this.AddIncidentInMemory (inMemory : Hashtable) =
    this.AddSingleton<Get>(find inMemory) |> ignore
    this.AddSingleton<Add>(save inMemory) |> ignore
